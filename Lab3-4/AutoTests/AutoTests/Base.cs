﻿using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Configuration;

namespace AutoTests
{
    public class Base
    {
        public static IWebDriver driver;

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver("driver");
            driver.Url = "https://makeup.com.ua/";
        }

        [TearDown]
        public void TearDown()
        {
            if (TestContext.CurrentContext.Result.Outcome != ResultState.Success)
            {
                string path = @"D:\Natasha\Labs\testing\Lab3-4\results\";
                var imageName = $"results_{DateTime.Now:yyyy-MM-dd_HH-mm-ss.fffff}.png";
                var screenshot = ((ITakesScreenshot)driver).GetScreenshot();
                screenshot.SaveAsFile(path + imageName);
            }

            //driver.Close();
            driver.Quit();
        }
    }
}
