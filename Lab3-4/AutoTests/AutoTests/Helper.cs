﻿using OpenQA.Selenium;
using System;

namespace AutoTests
{
    public static class Helper
    {
        public static IWebDriver driver;
        public static void AddItemToBasket()
        {
            driver = Base.driver;
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            driver.Navigate().GoToUrl("https://makeup.com.ua/product/837444/");
            driver.FindElement(By.CssSelector(".button.buy")).Click();
            Console.Write("product is added");
        }
    }
}
