﻿using OpenQA.Selenium;

namespace AutoTests
{
    public class DeliveryPage
    {
        IWebDriver driver;
        public DeliveryPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public IWebElement CityInput => driver.FindElement(By.Id("city"));
    }
}
