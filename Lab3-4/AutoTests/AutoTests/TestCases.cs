﻿using Allure.Commons;
using AutoTests;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AutoTest
{
    [TestFixture]
    [AllureNUnit]
    public class TestCases : Base
    {
        [Test]
        [AllureStory]
        [AllureTag("NUnit")]
        [AllureLink("https://github.com/unickq/allure-nunit")]
        [Category("Faker"), Description("This is test case using fake test data")]
        public void UseFaker()
        {
            try
            {
                driver.FindElement(By.CssSelector(".header-top div[data-popup-handler='auth']")).Click();
            }
            catch
            {
                driver.FindElement(By.CssSelector(".header-middle div[data-popup-handler='auth']")).Click();
            }

            var email = Faker.User.Email();
            var password = Faker.User.Password();

            driver.FindElement(By.CssSelector("input[name='user_login']")).SendKeys(email);
            driver.FindElement(By.CssSelector("input[name='user_pw']")).SendKeys(password);
            driver.FindElement(By.CssSelector("#form-auth button[type='submit']")).Click();

            Assert.IsNotNull(driver.FindElement(By.CssSelector(".form-inner-wrap")));

            ((ITakesScreenshot)driver).GetScreenshot().SaveAsFile(@"D:\Natasha\Labs\testing\Lab3-4\AutoTests\allure-results");
            AllureLifecycle.Instance.AddAttachment(@"D:\Natasha\Labs\testing\Lab3-4\AutoTests\allure-results", "name");
        }

        [Test]
        public void AddOneItemTest()
        {
            Helper.AddItemToBasket();
            var quantity = driver.FindElement(By.CssSelector("input[name='count[]']")).GetAttribute("value");
            Assert.AreEqual("1", quantity);
        }

        [Test]
        public void IncreaseQuantityTest()
        {
            Helper.AddItemToBasket();
            for (var i = 0; i < 5; i++)
            {
                driver.FindElement(By.CssSelector(".product__button-increase")).Click();
                Thread.Sleep(1000);
            }

            var quantity = driver.FindElement(By.CssSelector("input[name='count[]']")).GetAttribute("value");
            Assert.AreEqual("6", quantity);
        }

        [Test]
        public void RemoveItemFromTheCartTest()
        {
            Helper.AddItemToBasket();
            driver.FindElement(By.CssSelector(".product__button-remove")).Click();
            Thread.Sleep(1000);
            Assert.IsNotNull(driver.FindElement(By.CssSelector(".popup.cart.ng-hide")));
        }

        [Test]
        public void RecommendedProductsListTest()
        {
            Helper.AddItemToBasket();
            Assert.IsNotNull(driver.FindElement(By.CssSelector(".product-additional")));
        }

        [Test]
        public void PDPRedirectTest()
        {
            Helper.AddItemToBasket();

            var expectedProductId = driver.FindElement(By.CssSelector(".product-item__buy")).GetAttribute("data-id");

            driver.Navigate().GoToUrl("https://makeup.com.ua/");
            driver.FindElement(By.CssSelector(".header-basket")).Click();
            Thread.Sleep(1000);
            driver.FindElement(By.CssSelector(".product__header")).Click();

            var productId = driver.FindElement(By.CssSelector(".product-item__buy")).GetAttribute("data-id");
            Assert.AreEqual(expectedProductId, productId);
        }

        [Test]
        public void ContinueShoppingLinkTest()
        {
            Helper.AddItemToBasket();
            driver.FindElement(By.CssSelector(".cart-controls .link")).Click();
            Thread.Sleep(1000);
            Assert.IsNotNull(driver.FindElement(By.CssSelector(".popup.cart.ng-hide")));
        }

        [Test]
        public void GoToCheckoutPageTest()
        {
            Helper.AddItemToBasket();
            driver.FindElement(By.CssSelector(".cart-controls .button")).Click();
            Thread.Sleep(1000);
            Assert.IsNotNull(driver.FindElement(By.CssSelector(".layout.page-checkout")));
        }
    }
}
